<?php

require_once("animal.php");
require_once("ape.php");
require_once("frog.php");

$sheep = new Animal("shaun");

echo "<h2> RELASE 0 </h2>";
echo "Name = " . $sheep->name . "<br>"; 
echo "legs = " . $sheep->legs . "<br>"; 
echo "cold_blooded = " . $sheep->cold_blooded . "<br>"; 

echo "<h2> RELASE 1 </h2>";
$kodok = new frog("Buduk");
echo "Name = " . $kodok->name . "<br>"; 
echo "legs = " . $kodok->legs . "<br>"; 
echo "cold_blooded = " . $kodok->cold_blooded . "<br>"; 
echo $kodok->jump() . "<br><br>";

$sungokong = new Ape("Kera Sakti");
echo "Name = " . $sungokong->name . "<br>"; 
echo "legs = " . $sungokong->legs . "<br>"; 
echo "cold_blooded = " . $sungokong->cold_blooded . "<br>"; 
echo $sungokong->yell() . "<br>";

 ?>